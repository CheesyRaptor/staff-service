package com.somegroup.staffservice.service.impl;

import com.somegroup.staffservice.entity.Position;
import com.somegroup.staffservice.exception.EntityDoesNotExistException;
import com.somegroup.staffservice.exception.EntityIsAlreadyCreatedException;
import com.somegroup.staffservice.exception.EntityIsAttached;
import com.somegroup.staffservice.messaging.topic.PositionTopic;
import com.somegroup.staffservice.projection.PositionProjection;
import com.somegroup.staffservice.repository.PositionRepository;
import com.somegroup.staffservice.service.PositionService;
import com.somegroup.staffservice.service.StaffService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PositionServiceImpl implements PositionService {
    private final PositionRepository positionRepository;
    private final StaffService staffService;
    private final PositionTopic positionTopic;

    @Override
    public Optional<PositionProjection> getPosition(Long id) {
        return positionRepository.getById(id);
    }

    @Transactional
    @Override
    public PositionProjection createPosition(Position position) {
        if (position.getId() != null) {
            throw new EntityIsAlreadyCreatedException();
        }

        var positionId = positionRepository.save(position).getId();

        var positionProjection = positionRepository
                .getById(positionId)
                .orElseThrow(IllegalStateException::new);

        positionTopic.sendPositionCreatedMessage(positionProjection);

        return positionProjection;
    }

    @Override
    public PositionProjection updatePosition(Position position) {
        if (position.getId() == null) {
            throw new IllegalStateException("No id specified for update");
        }

        if (!positionRepository.existsById(position.getId())) {
            throw new EntityDoesNotExistException();
        }

        positionRepository.save(position);

        var positionProjection = positionRepository
                .getById(position.getId())
                .orElseThrow();

        positionTopic.sendPositionUpdatedMessage(positionProjection);

        return positionProjection;
    }

    @Override
    public void deletePosition(Long id) {
        var staffCount = staffService.countStaffAttachedToPosition(id);

        if (staffCount > 0) {
            throw new EntityIsAttached(String
                    .format("There are %d staff that have this position. "
                            + "Assign them other positions to delete this one.", staffCount));
        }

        var positionProjection = positionRepository.getById(id);

        positionRepository.deleteById(id);

        positionTopic.sendPositionDeletedMessage(positionProjection.orElseThrow());
    }

    @Override
    public Page<PositionProjection> getPage(Pageable pageable) {
        return positionRepository.findBy(pageable);
    }
}
