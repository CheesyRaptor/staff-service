package com.somegroup.staffservice.service.impl;

import com.somegroup.staffservice.entity.Staff;
import com.somegroup.staffservice.exception.EntityDoesNotExistException;
import com.somegroup.staffservice.exception.EntityIsAlreadyCreatedException;
import com.somegroup.staffservice.mapper.StaffMapper;
import com.somegroup.staffservice.messaging.topic.StaffTopic;
import com.somegroup.staffservice.projection.StaffProjection;
import com.somegroup.staffservice.repository.StaffRepository;
import com.somegroup.staffservice.service.StaffService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@AllArgsConstructor
public class StaffServiceImpl implements StaffService {
    private final StaffRepository staffRepository;
    private final StaffTopic staffTopic;
    private final StaffMapper staffMapper;

    @Override
    public Optional<StaffProjection> getStaffById(Long id) {
        return staffRepository.getStaffById(id);
    }

    @Transactional
    @Override
    public StaffProjection createStaff(Staff staff) {
        if (staff.getId() != null) {
            throw new EntityIsAlreadyCreatedException();
        }

        var savedStaff = staffRepository.save(staff);

        var staffProjection = staffRepository
                .getStaffById(savedStaff.getId())
                .orElseThrow(IllegalStateException::new);

        staffTopic.sendStaffCreatedMessage(staffMapper.map(staffProjection));
        return staffProjection;
    }

    @Transactional
    @Override
    public StaffProjection updateStaff(Staff staff) {
        if (staff.getId() == null) {
            throw new IllegalStateException("No id specified for update");
        }

        if (!staffRepository.existsById(staff.getId())) {
            throw new EntityDoesNotExistException();
        }

        staffRepository.save(staff);

        var staffProjection = staffRepository
                .getStaffById(staff.getId())
                .orElseThrow(IllegalStateException::new);

        staffTopic.sendStaffUpdatedMessage(staffMapper.map(staffProjection));

        return staffProjection;
    }

    @Override
    public void deleteStaff(Long id) {
        var staffProjection = staffRepository.getStaffById(id);
        staffRepository.deleteById(id);

        staffTopic.sendStaffDeletedMessage(staffProjection
                .map(staffMapper::map)
                .orElseThrow());
    }

    @Override
    public Page<StaffProjection> getPage(Pageable pageable) {
        return staffRepository.findBy(pageable);
    }

    @Override
    public long countStaffAttachedToPosition(Long positionId) {
        return staffRepository.countStaffByPositionId(positionId);
    }
}
