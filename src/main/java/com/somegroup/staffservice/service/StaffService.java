package com.somegroup.staffservice.service;

import com.somegroup.staffservice.entity.Staff;
import com.somegroup.staffservice.projection.StaffProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;


public interface StaffService {
    Optional<StaffProjection> getStaffById(Long id);

    StaffProjection createStaff(Staff staff);

    StaffProjection updateStaff(Staff staff);

    void deleteStaff(Long id);

    Page<StaffProjection> getPage(Pageable pageable);

    long countStaffAttachedToPosition(Long positionId);
}
