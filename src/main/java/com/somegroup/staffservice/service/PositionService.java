package com.somegroup.staffservice.service;

import com.somegroup.staffservice.entity.Position;
import com.somegroup.staffservice.projection.PositionProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface PositionService {
    Optional<PositionProjection> getPosition(Long id);

    PositionProjection createPosition(Position position);

    PositionProjection updatePosition(Position position);

    void deletePosition(Long id);

    Page<PositionProjection> getPage(Pageable pageable);
}
