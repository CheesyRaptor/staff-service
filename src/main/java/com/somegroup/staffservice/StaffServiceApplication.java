package com.somegroup.staffservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

//TODO? создать модуль для дто и интефейсами для контроллеров для feign client других сервисов
//TODO префиксы + разные схемы
@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class StaffServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StaffServiceApplication.class, args);
	}

}
