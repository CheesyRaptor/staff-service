package com.somegroup.staffservice.projection;

import com.somegroup.staffservice.entity.enums.StaffStatus;

import java.time.LocalDate;

public interface StaffProjection {
    Long getId();

    String getSurname();

    String getName();

    String getPatronymic();

    String getPassport();

    LocalDate getBirthday();

    LocalDate getHireDate();

    Long getPositionId();

    String getPositionName();

    StaffStatus getStaffStatus();
}
