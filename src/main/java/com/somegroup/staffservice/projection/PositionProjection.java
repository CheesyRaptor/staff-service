package com.somegroup.staffservice.projection;

public interface PositionProjection {
    Long getId();

    String getName();
}
