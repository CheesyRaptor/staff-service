package com.somegroup.staffservice.repository;

import com.somegroup.staffservice.entity.Staff;
import com.somegroup.staffservice.projection.StaffProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StaffRepository extends JpaRepository<Staff, Long> {
    Optional<StaffProjection> getStaffById(Long id);

    Page<StaffProjection> findBy(Pageable pageable);

    long countStaffByPositionId(Long positionId);
}
