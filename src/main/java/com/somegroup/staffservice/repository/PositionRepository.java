package com.somegroup.staffservice.repository;


import com.somegroup.staffservice.entity.Position;
import com.somegroup.staffservice.projection.PositionProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PositionRepository extends JpaRepository<Position, Long> {
    Optional<PositionProjection> getById(Long id);

    Page<PositionProjection> findBy(Pageable pageable);
}
