package com.somegroup.staffservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.somegroup.staffservice.entity.enums.StaffStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class StaffDto {
    Long id;
    String surname;
    String name;
    String patronymic;
    String passport;
    LocalDate birthday;
    LocalDate hireDate;
    Long positionId;
    StaffStatus staffStatus;
}
