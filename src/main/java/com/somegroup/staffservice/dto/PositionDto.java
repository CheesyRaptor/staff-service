package com.somegroup.staffservice.dto;

import com.somegroup.staffservice.projection.PositionProjection;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PositionDto implements PositionProjection {
    Long id;
    String name;
}
