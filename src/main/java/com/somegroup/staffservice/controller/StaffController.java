package com.somegroup.staffservice.controller;

import com.somegroup.staffservice.entity.Staff;
import com.somegroup.staffservice.projection.StaffProjection;
import com.somegroup.staffservice.service.StaffService;
import com.somegroup.staffservice.utils.Pager;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/staff")
@AllArgsConstructor
@PreAuthorize("hasRole('ADMIN')")
public class StaffController {
    private final StaffService staffService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<StaffProjection> getStaff(@PathVariable Long id) {
        return staffService
                .getStaffById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<StaffProjection> getPage(Pager pager) {
        return staffService.getPage(pager.getPageable());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    StaffProjection createStaff(@RequestBody @Valid Staff staff) {
        return staffService.createStaff(staff);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    StaffProjection updateStaff(@PathVariable Long id, @RequestBody @Valid Staff staff) {
        staff.setId(id);
        return staffService.updateStaff(staff);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    void deleteStaff(@PathVariable Long id) {
        try {
            staffService.deleteStaff(id);
        } catch (EmptyResultDataAccessException e) {
            //if there is no staff with given id
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }
}
