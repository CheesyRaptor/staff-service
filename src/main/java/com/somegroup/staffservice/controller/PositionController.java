package com.somegroup.staffservice.controller;

import com.somegroup.staffservice.entity.Position;
import com.somegroup.staffservice.projection.PositionProjection;
import com.somegroup.staffservice.service.PositionService;
import com.somegroup.staffservice.utils.Pager;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@RestController
@AllArgsConstructor
@RequestMapping("api/v1/positions")
@PreAuthorize("hasRole('ADMIN')")
public class PositionController {

    private final PositionService positionService;

    @GetMapping("/{id}")
    ResponseEntity<PositionProjection> getPosition(@PathVariable Long id) {
        return positionService
                .getPosition(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    PositionProjection createPosition(@RequestBody @Valid Position position) {
        return positionService.createPosition(position);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    PositionProjection updatePosition(@PathVariable Long id, @RequestBody @Valid Position position) {
        position.setId(id);
        return positionService.updatePosition(position);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    void deletePosition(@PathVariable Long id) {
        try {
            positionService.deletePosition(id);
        } catch (EmptyResultDataAccessException e) {
            //if there is no position with given id
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Page<PositionProjection> getPage(Pager pager) {
        return positionService.getPage(pager.getPageable());
    }
}
