package com.somegroup.staffservice.entity.enums;

public enum StaffStatus {
    ACTIVE, FIRED
}
