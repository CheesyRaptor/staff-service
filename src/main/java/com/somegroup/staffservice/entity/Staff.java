package com.somegroup.staffservice.entity;

import com.somegroup.staffservice.entity.enums.StaffStatus;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Size(max = 50)
    private String surname;

    @NotEmpty
    @Size(max = 50)
    private String name;

    @Size(max = 50)
    private String patronymic;

    @NotEmpty
    @Size(max = 200)
    private String passport;

    @Past
    @NotNull
    private LocalDate birthday;

    @NotNull
    private LocalDate hireDate;

    @NotNull
    @ManyToOne
    @JoinColumn
    private Position position;

    @NotNull
    @Enumerated(EnumType.STRING)
    private StaffStatus staffStatus;

    @AssertTrue
    public boolean isValidHireAndFireDate() {
        return birthday != null && hireDate != null && birthday.isBefore(hireDate);
    }
}
