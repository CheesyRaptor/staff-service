package com.somegroup.staffservice.mapper;

import com.somegroup.staffservice.dto.StaffDto;
import com.somegroup.staffservice.projection.StaffProjection;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StaffMapper {
    StaffDto map(StaffProjection staffProjection);
}
