package com.somegroup.staffservice.utils;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import java.util.Map;
import java.util.Optional;

import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toUnmodifiableList;

@Value
@Builder
public class Pager {
    //Any sorting is needed when using pagination
    static public final Sort.Order DEFAULT_ORDER = Sort.Order.asc("id");
    static public final int DEFAULT_PAGE = 0;
    static public final int DEFAULT_PAGE_SIZE = 10;

    @Builder.Default
    int page = DEFAULT_PAGE;

    @Builder.Default
    int pageSize = DEFAULT_PAGE_SIZE;

    //Lombok builder keeps insertion order
    @Singular("addPropertyOrder")
    Map<String, Direction> propertiesOrder;

    public Pageable getPageable() {
        return getPageable(emptyMap());
    }

    public Pageable getPageable(Map<String, String> mapDtoFieldsToQueryProperties) {
        if (propertiesOrder.isEmpty()) {
            return PageRequest.of(page, pageSize, Sort.by(DEFAULT_ORDER));
        }

        var orders = propertiesOrder
                .entrySet()
                .stream()
                .map(it -> {
                    var dtoField = it.getKey();
                    var queryProperty = mapDtoFieldsToQueryProperties.getOrDefault(dtoField, dtoField);

                    return Optional
                            .ofNullable(it.getValue())
                            .map(direction -> switch (it.getValue()) {
                                case ASC -> Sort.Order.asc(queryProperty);
                                case DESC -> Sort.Order.desc(queryProperty);
                            })
                            .orElse(Sort.Order.by(queryProperty));
                })
                .collect(toUnmodifiableList());

        return PageRequest.of(page, pageSize, Sort.by(orders));
    }

}
