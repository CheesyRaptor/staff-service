package com.somegroup.staffservice.messaging;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import java.util.Map;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class MessagingUtils {
    public static <T> Message<MessageDto<T>> prepareMessage(T data, Action action, String key) {
        return new GenericMessage<>(new MessageDto<>(data, action), Map.of(KafkaHeaders.MESSAGE_KEY, key));
    }
}
