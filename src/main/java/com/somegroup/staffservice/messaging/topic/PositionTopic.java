package com.somegroup.staffservice.messaging.topic;

import com.somegroup.staffservice.messaging.Action;
import com.somegroup.staffservice.messaging.MessagingUtils;
import com.somegroup.staffservice.projection.PositionProjection;
import lombok.AllArgsConstructor;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public final class PositionTopic {
    private final StreamBridge streamBridge;
    private final static String POSITION_OUT = "position-out-0";

    private void sendPositionMessage(PositionProjection position, Action action) {
        var message = MessagingUtils.prepareMessage(position, action, position.getId().toString());
        streamBridge.send(POSITION_OUT, message);
    }

    public void sendPositionCreatedMessage(PositionProjection position) {
        sendPositionMessage(position, Action.CREATE);
    }

    public void sendPositionUpdatedMessage(PositionProjection position) {
        sendPositionMessage(position, Action.UPDATE);
    }

    public void sendPositionDeletedMessage(PositionProjection position) {
        sendPositionMessage(position, Action.DELETE);
    }

}
