package com.somegroup.staffservice.messaging.topic;

import com.somegroup.staffservice.dto.StaffDto;
import com.somegroup.staffservice.messaging.Action;
import com.somegroup.staffservice.messaging.MessagingUtils;
import lombok.AllArgsConstructor;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public final class StaffTopic {
    private final StreamBridge streamBridge;
    private final static String STAFF_OUT = "staff-out-0";

    private void sendStaffMessage(StaffDto staff, Action action) {
        var message = MessagingUtils.prepareMessage(staff, action, staff.getId().toString());
        streamBridge.send(STAFF_OUT, message);
    }

    public void sendStaffCreatedMessage(StaffDto staff) {
        sendStaffMessage(staff, Action.CREATE);
    }

    public void sendStaffUpdatedMessage(StaffDto staff) {
        sendStaffMessage(staff, Action.UPDATE);
    }

    public void sendStaffDeletedMessage(StaffDto staff) {
        sendStaffMessage(staff, Action.DELETE);
    }
}
