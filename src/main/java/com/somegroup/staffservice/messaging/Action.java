package com.somegroup.staffservice.messaging;

public enum Action {
    CREATE, UPDATE, DELETE
}
