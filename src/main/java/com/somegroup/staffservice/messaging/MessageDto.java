package com.somegroup.staffservice.messaging;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class MessageDto<T> {
    T data;
    Action action;
}
