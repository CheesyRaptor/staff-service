package com.somegroup.staffservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EntityIsAlreadyCreatedException extends IllegalStateException {
    public EntityIsAlreadyCreatedException() {
        super("Entity is already created");
    }
}
