package com.somegroup.staffservice.argumentresolver;

import com.somegroup.staffservice.utils.Pager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Sort;
import org.springframework.data.util.StreamUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toUnmodifiableList;

@Slf4j
public class PagerArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterType().equals(Pager.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter,
                                  ModelAndViewContainer modelAndViewContainer,
                                  NativeWebRequest nativeWebRequest,
                                  WebDataBinderFactory webDataBinderFactory) {
        var pageBuilder = Pager.builder();

        extractInt(nativeWebRequest, "page").ifPresent(pageBuilder::page);
        extractInt(nativeWebRequest, "pageSize").ifPresent(pageBuilder::pageSize);

        var properties = extractStringList(nativeWebRequest, "properties");
        var directionStrings = extractStringList(nativeWebRequest, "directions");

        if (properties.size() != directionStrings.size()) {
            var e = new IllegalStateException("properties list should have same size as directions list");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

        List<Sort.Direction> directions;

        try {
            directions = directionStrings
                    .stream()
                    .map(Sort.Direction::fromString)
                    .collect(toUnmodifiableList());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

        var propertiesOrder = StreamUtils
                .zip(properties.stream(), directions.stream(), Map::entry)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        return pageBuilder.propertiesOrder(propertiesOrder).build();
    }

    private List<String> extractStringList(NativeWebRequest nativeWebRequest, String parameter) {
        return ofNullable(nativeWebRequest.getParameter(parameter))
                .map(it -> it.split(","))
                .stream()
                .flatMap(Arrays::stream)
                .map(String::trim)
                .collect(toUnmodifiableList());
    }

    private Optional<Integer> extractInt(NativeWebRequest nativeWebRequest, String parameter) {
        try {
            return ofNullable(nativeWebRequest.getParameter(parameter)).map(Integer::parseInt);
        } catch (NumberFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }
}
