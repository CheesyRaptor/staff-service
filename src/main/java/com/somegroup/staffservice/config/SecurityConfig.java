package com.somegroup.staffservice.config;

import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@EnableWebSecurity
@Profile("dev")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
    private final JwtGrantedAuthoritiesConverter defaultJwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
    private final JwtGrantedAuthoritiesConverter roleJwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        roleJwtGrantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
        roleJwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("roles");

        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwt -> {
            var defaultGrantedAuthorities = defaultJwtGrantedAuthoritiesConverter.convert(jwt);
            var roleGrantedAuthorities = roleJwtGrantedAuthoritiesConverter.convert(jwt);
            return Stream
                    .concat(defaultGrantedAuthorities.stream(), roleGrantedAuthorities.stream())
                    .collect(Collectors.toUnmodifiableList());
        });

        http.csrf().disable()
                .authorizeRequests().antMatchers("actuator", "/actuator/**").permitAll().and()
                .authorizeRequests().anyRequest().authenticated()
                .and()
                .oauth2ResourceServer()
                .jwt()
                .jwtAuthenticationConverter(jwtAuthenticationConverter);
    }
}
