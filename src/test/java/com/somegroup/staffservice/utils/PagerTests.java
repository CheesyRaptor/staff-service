package com.somegroup.staffservice.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Sort;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class PagerTests {

    @Test
    @DisplayName("Creates Pageable with default id ordering")
    void pagerWithIdOrdering() {
        var pager = Pager
                .builder()
                .page(2)
                .pageSize(8)
                .build();

        var pageable = pager.getPageable();

        assertThat(pageable.getPageNumber()).isEqualTo(2);
        assertThat(pageable.getPageSize()).isEqualTo(8);
        assertThat(pageable.getSort()).containsExactly(Pager.DEFAULT_ORDER);
    }

    @Test
    @DisplayName("Creates Pageable with custom ordering")
    void pagerWithCustomOrdering() {
        var pager = Pager
                .builder()
                .page(25)
                .pageSize(10)
                .addPropertyOrder("name", Sort.Direction.ASC)
                .addPropertyOrder("position.name", Sort.Direction.DESC)
                .build();

        var pageable = pager.getPageable();

        assertThat(pageable.getPageNumber()).isEqualTo(25);
        assertThat(pageable.getPageSize()).isEqualTo(10);
        assertThat(pageable.getSort())
                .containsExactly(Sort.Order.asc("name"), Sort.Order.desc("position.name"));
    }

    @Test
    @DisplayName("Creates Pageable using orders and mappings")
    void pagerWithCustomOrderingAndPropertyMapping() {
        var pager = Pager
                .builder()
                .page(25)
                .pageSize(10)
                .addPropertyOrder("name", Sort.Direction.ASC)
                .addPropertyOrder("positionName", Sort.Direction.DESC)
                .build();

        var pageable = pager.getPageable(Map.of("positionName", "position.name"));

        assertThat(pageable.getPageNumber()).isEqualTo(25);
        assertThat(pageable.getPageSize()).isEqualTo(10);
        assertThat(pageable.getSort())
                .containsExactly(Sort.Order.asc("name"), Sort.Order.desc("position.name"));
    }

}
