package com.somegroup.staffservice.argumentresolver;

import com.somegroup.staffservice.utils.Pager;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PagerArgumentResolverTest {
    private final PagerArgumentResolver pagerArgumentResolver = new PagerArgumentResolver();

    @Mock
    private MethodParameter methodParameter;

    @Mock
    private ModelAndViewContainer modelAndViewContainer;

    @Mock
    private NativeWebRequest nativeWebRequest;

    @Mock
    private WebDataBinderFactory webDataBinderFactory;

    private void setupNativeWebRequest(String page, String pageSize, String properties, String directions) {
        when(nativeWebRequest.getParameter("page")).thenReturn(page);
        when(nativeWebRequest.getParameter("pageSize")).thenReturn(pageSize);
        when(nativeWebRequest.getParameter("properties")).thenReturn(properties);
        when(nativeWebRequest.getParameter("directions")).thenReturn(directions);
    }

    private Pager resolveAndReturn() {
        var result = pagerArgumentResolver
                .resolveArgument(methodParameter,
                        modelAndViewContainer,
                        nativeWebRequest,
                        webDataBinderFactory);

        verifyNoInteractions(methodParameter, modelAndViewContainer, webDataBinderFactory);

        assertThat(result)
                .isNotNull()
                .isInstanceOf(Pager.class);

        return (Pager) result;
    }

    @Test
    @DisplayName("All arguments are specified")
    void creationTest() {
        setupNativeWebRequest("0", "10", "surname, position.name", "asc, desc");
        var pager = resolveAndReturn();

        assertThat(pager.getPage()).isEqualTo(0);
        assertThat(pager.getPageSize()).isEqualTo(10);
        assertThat(pager.getPropertiesOrder())
                .containsExactly(Map.entry("surname", Sort.Direction.ASC),
                        Map.entry("position.name", Sort.Direction.DESC));
    }

    @Test
    @DisplayName("Only page and page size specified")
    void creationWithDefaultSorting() {
        setupNativeWebRequest("0", "10", null, null);

        var pager = resolveAndReturn();
        assertThat(pager.getPage()).isEqualTo(0);
        assertThat(pager.getPageSize()).isEqualTo(10);
        assertThat(pager.getPropertiesOrder()).isEmpty();
    }

    @Test
    @DisplayName("Default pager creation")
    void createDefaultPager() {
        setupNativeWebRequest(null, null, null, null);
        var pager = resolveAndReturn();

        assertThat(pager.getPage()).isEqualTo(Pager.DEFAULT_PAGE);
        assertThat(pager.getPageSize()).isEqualTo(Pager.DEFAULT_PAGE_SIZE);
        assertThat(pager.getPropertiesOrder()).isEmpty();
    }

    @Test
    @DisplayName("When properties and directions sizes are not same throws exception")
    void throwsExceptionOnDifferentSizes() {
        setupNativeWebRequest("1", "20", "a,b,c", "asc");

        Assertions
                .assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(this::resolveAndReturn)
                .withCauseInstanceOf(IllegalStateException.class)
                .withMessageContaining("properties list should have same size as directions list");
    }

    @Test
    @DisplayName("When wrong directions specified throws exception")
    void throwsExceptionOnWrongDirections() {
        setupNativeWebRequest("1", "20", "a,b,c", "a1sc, asc, asc");

        Assertions
                .assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(this::resolveAndReturn)
                .withCauseInstanceOf(IllegalArgumentException.class)
                .withMessageContaining("Invalid value 'a1sc' for orders given! Has to be either 'desc' or 'asc' (case insensitive).");
    }

    @Test
    @DisplayName("When page is not Integer throws exception")
    void throwsExceptionOnWrongPage() {
        when(nativeWebRequest.getParameter("page")).thenReturn("abc");

        Assertions
                .assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(this::resolveAndReturn)
                .withCauseInstanceOf(NumberFormatException.class);
    }

    @Test
    @DisplayName("When pageSize is not Integer throws exception")
    void throwsExceptionOnWrongPageSize() {
        when(nativeWebRequest.getParameter("page")).thenReturn(null);
        when(nativeWebRequest.getParameter("pageSize")).thenReturn("abc");

        Assertions
                .assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(this::resolveAndReturn)
                .withCauseInstanceOf(NumberFormatException.class);
    }
}
