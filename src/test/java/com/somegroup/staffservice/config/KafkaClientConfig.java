package com.somegroup.staffservice.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.somegroup.staffservice.dto.PositionDto;
import com.somegroup.staffservice.dto.StaffDto;
import com.somegroup.staffservice.messaging.MessageDto;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.List;

@Configuration
public class KafkaClientConfig {

    @Autowired
    private KafkaProperties kafkaProperties;

    @Bean
    public AdminClient getAdminClient() {
        return KafkaAdminClient.create(kafkaProperties.buildAdminProperties());
    }

    @Bean
    public NewTopic getStaffTopic() {
        return TopicBuilder.name("staff-topic").compact().build();
    }

    @Bean
    public NewTopic getPositionTopic() {
        return TopicBuilder.name("position-topic").compact().build();
    }

    @Bean
    public KafkaConsumer<String, MessageDto<StaffDto>> getKafkaStaffConsumer() {
        var consumer = new KafkaConsumer<>(kafkaProperties.buildConsumerProperties(),
                new StringDeserializer(),
                new JsonDeserializer<>(new TypeReference<MessageDto<StaffDto>>() {
                }));
        consumer.assign(List.of(new TopicPartition("staff-topic", 0)));
        return consumer;
    }

    @Bean
    public KafkaConsumer<String, MessageDto<PositionDto>> getKafkaPositionConsumer() {
        var consumer = new KafkaConsumer<>(kafkaProperties.buildConsumerProperties(),
                new StringDeserializer(),
                new JsonDeserializer<>(new TypeReference<MessageDto<PositionDto>>() {
                }));
        consumer.assign(List.of(new TopicPartition("position-topic", 0)));
        return consumer;
    }
}
