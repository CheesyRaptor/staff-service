package com.somegroup.staffservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.somegroup.staffservice.IntegrationTests;
import com.somegroup.staffservice.TestUtils;
import com.somegroup.staffservice.dto.PositionDto;
import com.somegroup.staffservice.entity.Position;
import com.somegroup.staffservice.entity.Staff;
import com.somegroup.staffservice.exception.EntityDoesNotExistException;
import com.somegroup.staffservice.exception.EntityIsAlreadyCreatedException;
import com.somegroup.staffservice.exception.EntityIsAttached;
import com.somegroup.staffservice.messaging.Action;
import com.somegroup.staffservice.messaging.MessageDto;
import com.somegroup.staffservice.repository.PositionRepository;
import com.somegroup.staffservice.repository.StaffRepository;
import com.somegroup.staffservice.service.PositionService;
import com.somegroup.staffservice.service.StaffService;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.time.LocalDate;
import java.util.stream.IntStream;

import static com.somegroup.staffservice.entity.enums.StaffStatus.ACTIVE;
import static java.util.stream.Collectors.toUnmodifiableList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PositionControllerTest extends IntegrationTests {
    private final String POSITION_API = "/api/v1/positions";
    private final String GET_POSITION = POSITION_API + "/{0}";
    private final String PUT_POSITION = POSITION_API + "/{0}";
    private final String DELETE_POSITION = POSITION_API + "/{0}";
    private final String GET_POSITION_PAGE = POSITION_API + "?page={0}&pageSize={1}";

    private final String positionJson = "{\"id\": %d, \"name\": \"%s\"}";
    private final String positionCreationJson = "{\"name\": \"%s\"}";

    @Autowired
    private PositionService positionService;

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private StaffService staffService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private KafkaConsumer<String, MessageDto<PositionDto>> kafkaConsumer;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AdminClient adminClient;

    private final TopicPartition positionTopic = new TopicPartition("position-topic", 0);

    private ResultMatcher testKafkaMessage(Action action) {
        return result -> testKafkaMessage(action, result.getResponse().getContentAsString()).match(result);
    }

    private ResultMatcher testKafkaMessage(Action action, String responseString) {
        return TestUtils
                .latestMessageMatcher(action,
                        responseString,
                        kafkaConsumer,
                        adminClient,
                        positionTopic,
                        objectMapper);
    }

    @AfterEach
    void cleanUp() {
        staffRepository.deleteAll();
        positionRepository.deleteAll();
    }

    String getPositionJson(Long id, String name) {
        return String.format(positionJson, id, name);
    }

    String getPositionCreationJson(String name) {
        return String.format(positionCreationJson, name);
    }

    @Nested
    @DisplayName("Testing GET " + POSITION_API)
    @WithMockUser(roles = "ADMIN")
    class GetTests {

        @Test
        @DisplayName("Can get existing position")
        void canGetExisting() throws Exception {
            var positionName = "position123321";
            var positionId = positionService
                    .createPosition(Position.builder().name(positionName).build())
                    .getId();

            mockMvc
                    .perform(get(GET_POSITION, positionId))
                    .andExpect(status().isOk())
                    .andExpect(content().json(getPositionJson(positionId, positionName)));
        }

        @Test
        @DisplayName("Returns 'not found' if position does not exist")
        void returnsNotFound() throws Exception {
            mockMvc
                    .perform(get(GET_POSITION, 123))
                    .andExpect(status().isNotFound());
        }
    }

    @Nested
    @DisplayName("Testing POST " + POSITION_API)
    @WithMockUser(roles = "ADMIN")
    class PostTests {

        @Test
        @DisplayName("Can create position")
        void canCreatePosition() throws Exception {
            var positionName = "position123321";

            mockMvc
                    .perform(post(POSITION_API)
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(getPositionCreationJson(positionName)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", notNullValue(Long.class), Long.class))
                    .andExpect(jsonPath("$.name", is(positionName)))
                    .andExpect(testKafkaMessage(Action.CREATE));
        }

        @Test
        @DisplayName("Position json is validated")
        void isValidated() throws Exception {
            mockMvc
                    .perform(post(POSITION_API)
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(getPositionCreationJson("")))
                    .andExpect(status().isBadRequest())
                    .andExpect(result -> assertThat(result.getResolvedException())
                            .isInstanceOf(MethodArgumentNotValidException.class));
        }

        @Test
        @DisplayName("Can not create position with specified id")
        void canNotCreateWithSpecifiedId() throws Exception {
            mockMvc
                    .perform(post(POSITION_API)
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(getPositionJson(1L, "position123")))
                    .andExpect(status().isBadRequest())
                    .andExpect(result -> assertThat(result.getResolvedException())
                            .isInstanceOf(EntityIsAlreadyCreatedException.class));
        }
    }

    @Nested
    @DisplayName("Testing PUT " + POSITION_API)
    @WithMockUser(roles = "ADMIN")
    class PutTests {

        @Test
        @DisplayName("Can update position")
        void canUpdatePosition() throws Exception {
            var positionId = positionService
                    .createPosition(Position.builder().name("position123").build())
                    .getId();

            var newPositionName = "pos123";

            mockMvc
                    .perform(put(PUT_POSITION, positionId)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(getPositionCreationJson(newPositionName)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(positionId), Long.class))
                    .andExpect(jsonPath("$.name", is(newPositionName)))
                    .andExpect(testKafkaMessage(Action.UPDATE));
        }

        @Test
        @DisplayName("Data is validated")
        void dataIsValidated() throws Exception {
            var positionId = positionService
                    .createPosition(Position.builder().name("position123").build())
                    .getId();

            var newPositionName = "";

            mockMvc
                    .perform(put(PUT_POSITION, positionId)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(getPositionCreationJson(newPositionName)))
                    .andExpect(status().isBadRequest())
                    .andExpect(result -> assertThat(result.getResolvedException())
                            .isInstanceOf(MethodArgumentNotValidException.class));
        }

        @Test
        @DisplayName("Returns 'bad request' when trying to update not existing entity")
        void badRequestWhenUpdatingNotExistingPosition() throws Exception {
            var newPositionName = "some-name";

            mockMvc
                    .perform(put(PUT_POSITION, 123L)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(getPositionCreationJson(newPositionName)))
                    .andExpect(status().isBadRequest())
                    .andExpect(result -> assertThat(result.getResolvedException())
                            .isInstanceOf(EntityDoesNotExistException.class));
        }
    }

    @Nested
    @DisplayName("Testing DELETE " + POSITION_API)
    @WithMockUser(roles = "ADMIN")
    class DeleteTest {

        @Test
        @DisplayName("Can delete position")
        void canDelete() throws Exception {
            var positionName = "pos123";

            var positionId = positionService
                    .createPosition(Position.builder().name(positionName).build())
                    .getId();

            var positionJson = JsonPath
                    .parse("{}")
                    .put("$", "id", positionId)
                    .put("$", "name", positionName)
                    .jsonString();

            mockMvc
                    .perform(delete(DELETE_POSITION, positionId))
                    .andExpect(status().isOk())
                    .andExpect(testKafkaMessage(Action.DELETE, positionJson));
        }

        @Test
        @DisplayName("Returns 'bad request' when staff is attached")
        void throwsWhenStaffAttached() throws Exception {
            var positionId = positionService
                    .createPosition(Position.builder().name("pos123").build())
                    .getId();

            staffService
                    .createStaff(Staff
                            .builder()
                            .surname("surname1")
                            .name("name1")
                            .birthday(LocalDate.of(1990, 1, 1))
                            .hireDate(LocalDate.of(2021, 1, 1))
                            .passport("pass123")
                            .position(Position.builder().id(positionId).build())
                            .staffStatus(ACTIVE)
                            .build());

            mockMvc
                    .perform(delete(DELETE_POSITION, positionId))
                    .andExpect(status().isBadRequest())
                    .andExpect(result -> assertThat(result.getResolvedException())
                            .isInstanceOf(EntityIsAttached.class));
        }

        @Test
        @DisplayName("Returns 'bad request' when position does not exist")
        void throwsWhenPositionDoesNotExist() throws Exception {
            mockMvc
                    .perform(delete(DELETE_POSITION, 123L))
                    .andExpect(status().isBadRequest())
                    .andExpect(result -> assertThat(result.getResolvedException())
                            .hasCauseInstanceOf(EmptyResultDataAccessException.class));
        }
    }

    @Nested
    @DisplayName("Testing GET page from " + POSITION_API)
    @WithMockUser(roles = "ADMIN")
    class GetPage {

        @Test
        @DisplayName("Can get pages with default ordering by id")
        void canGetPages() throws Exception {
            var idList = IntStream
                    .range(0, 10)
                    .map(i -> positionService
                            .createPosition(Position.builder().name("position" + i).build())
                            .getId()
                            .intValue())
                    .boxed()
                    .sorted()
                    .collect(toUnmodifiableList());

            //Default ordering by id and paging are working
            mockMvc
                    .perform(get(GET_POSITION_PAGE, 0, 5))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.totalPages", is(2)))
                    .andExpect(jsonPath("$.totalElements", is(10)))
                    .andExpect(jsonPath("$.number", is(0)))
                    .andExpect(jsonPath("$.size", is(5)))
                    .andExpect(jsonPath("$.content[*].id", contains(idList.subList(0, 5).toArray())));

            mockMvc
                    .perform(get(GET_POSITION_PAGE, 1, 5))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.totalPages", is(2)))
                    .andExpect(jsonPath("$.totalElements", is(10)))
                    .andExpect(jsonPath("$.number", is(1)))
                    .andExpect(jsonPath("$.size", is(5)))
                    .andExpect(jsonPath("$.content[*].id", contains(idList.subList(5, 10).toArray())));
        }
    }
}
