package com.somegroup.staffservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.somegroup.staffservice.IntegrationTests;
import com.somegroup.staffservice.TestUtils;
import com.somegroup.staffservice.dto.StaffDto;
import com.somegroup.staffservice.entity.Position;
import com.somegroup.staffservice.exception.EntityDoesNotExistException;
import com.somegroup.staffservice.exception.EntityIsAlreadyCreatedException;
import com.somegroup.staffservice.messaging.Action;
import com.somegroup.staffservice.messaging.MessageDto;
import com.somegroup.staffservice.projection.PositionProjection;
import com.somegroup.staffservice.repository.PositionRepository;
import com.somegroup.staffservice.repository.StaffRepository;
import com.somegroup.staffservice.service.PositionService;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.stream.IntStream;

import static com.somegroup.staffservice.TestUtils.getJson;
import static com.somegroup.staffservice.entity.enums.StaffStatus.FIRED;
import static java.util.stream.Collectors.toUnmodifiableList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class StaffControllerTest extends IntegrationTests {
    private static final String STAFF_API = "/api/v1/staff";
    private static final String GET_STAFF = STAFF_API + "/{0}";
    private static final String PUT_STAFF = STAFF_API + "/{0}";
    private static final String DELETE_STAFF = STAFF_API + "/{0}";
    private static final String GET_STAFF_PAGE = STAFF_API + "?page={0}&pageSize={1}";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PositionService positionService;

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private KafkaConsumer<String, MessageDto<StaffDto>> kafkaConsumer;

    @Autowired
    private AdminClient adminClient;

    private final TopicPartition staffTopic = new TopicPartition("staff-topic", 0);

    @Autowired
    private ObjectMapper objectMapper;

    private PositionProjection position;

    private PositionProjection createPosition() {
        return positionService.createPosition(Position.builder().name("position123").build());
    }

    private String createStaff(String staffJson) throws Exception {
        return mockMvc
                .perform(post(STAFF_API)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(staffJson))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    private ResultMatcher testKafkaMessage(Action action) {
        return result -> testKafkaMessage(action, result.getResponse().getContentAsString()).match(result);
    }


    //TODO Отрефачить
    private ResultMatcher testKafkaMessage(Action action, String responseString) {
        //PositionName property should not be in kafka message
        var contentAsString = JsonPath
                .parse(responseString)
                .delete("$.positionName")
                .jsonString();

        return TestUtils
                .latestMessageMatcher(action,
                        contentAsString,
                        kafkaConsumer,
                        adminClient,
                        staffTopic,
                        objectMapper);
    }

    @AfterEach
    public void cleanUp() {
        staffRepository.deleteAll();
        positionRepository.deleteAll();
    }

    @BeforeEach
    void initPosition() {
        position = createPosition();
    }

    @Nested
    @DisplayName("Testing POST " + STAFF_API)
    @WithMockUser(roles = {"ADMIN"})
    class StaffCreationTests {

        @Test
        @DisplayName("Can create valid staff")
        void canCreate(@Value("classpath:json/staff-create.json") Resource staff,
                       @Value("classpath:json/staff-create-return.json") Resource staffReturn) throws Exception {
            mockMvc
                    .perform(post(STAFF_API)
                            .contentType(APPLICATION_JSON_VALUE)
                            .content(getJson(staff, position.getId())))
                    .andExpect(status().isOk())
                    .andExpect(content().json(getJson(staffReturn, position.getId())))
                    .andExpect(testKafkaMessage(Action.CREATE));
        }

        @Test
        @DisplayName("Throws exception on invalid staff")
        void canValidate(@Value("classpath:json/staff-invalid.json") Resource invalidStaff) throws Exception {
            mockMvc
                    .perform(post(STAFF_API)
                            .contentType(APPLICATION_JSON_VALUE)
                            .content(getJson(invalidStaff, position.getId())))
                    .andExpect(status().is4xxClientError())
                    .andExpect(result -> assertThat(result.getResolvedException())
                            .isInstanceOf(MethodArgumentNotValidException.class));
        }

        @Test
        @DisplayName("Throws exception when trying to overwrite existing staff")
        void cannotOverwrite(@Value("classpath:json/staff-create.json") Resource staff) throws Exception {
            var staffJson = getJson(staff, position.getId());

            //creating staff
            var responseBody = createStaff(staffJson);
            var sameStaff = JsonPath
                    .parse(staffJson)
                    .put("$", "id", JsonPath.parse(responseBody).read("$.id"));

            //Trying to overwrite given staff
            mockMvc
                    .perform(post(STAFF_API)
                            .contentType(APPLICATION_JSON_VALUE)
                            .content(sameStaff.jsonString()))
                    .andExpect(status().is4xxClientError())
                    .andExpect(result -> assertThat(result.getResolvedException())
                            .isInstanceOf(EntityIsAlreadyCreatedException.class));
        }
    }

    @Nested
    @DisplayName("Testing GET " + STAFF_API)
    @WithMockUser(roles = {"ADMIN"})
    class StaffReadingTests {
        @Test
        @DisplayName("Can get existing staff")
        void canGetExistingStaff(@Value("classpath:json/staff-create.json") Resource staff,
                                 @Value("classpath:json/staff-create-return.json") Resource staffReturn) throws Exception {
            var staffId = JsonPath
                    .parse(createStaff(getJson(staff, position.getId())))
                    .read("$.id");

            mockMvc
                    .perform(get(GET_STAFF, staffId))
                    .andExpect(status().isOk())
                    .andExpect(content().json(getJson(staffReturn, position.getId())));
        }

        @Test
        @DisplayName("Returns \"not found\" when staff do not exist")
        void canNotReturnNotExistingStaff() throws Exception {
            mockMvc
                    .perform(get(GET_STAFF, 1))
                    .andExpect(status().isNotFound());
        }
    }

    @Nested
    @DisplayName("Testing PUT " + STAFF_API)
    @WithMockUser(roles = "ADMIN")
    class StaffUpdatingTest {

        private DocumentContext staffJson;

        @BeforeEach
        void setupStaff(@Value("classpath:json/staff-create.json") Resource staff) throws Exception {
            staffJson = JsonPath.parse(createStaff(getJson(staff, position.getId())));
        }

        @Test
        @DisplayName("Can update existing staff")
        void canUpdateStaff(@Value("classpath:json/staff-create-return.json") Resource staffReturn) throws Exception {
            //Change staff to fired status
            mapStaffJsonToFired();

            var staffId = staffJson.read("$.id");

            var expectedJson = JsonPath
                    .parse(getJson(staffReturn, position.getId()))
                    .set("$.staffStatus", FIRED.name())
                    .put("$", "id", staffId)
                    .jsonString();

            mockMvc
                    .perform(put(PUT_STAFF, staffId)
                            .content(staffJson.jsonString())
                            .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andExpect(content().json(expectedJson))
                    .andExpect(testKafkaMessage(Action.UPDATE));
        }

        private void mapStaffJsonToFired() {
            staffJson
                    .delete("$.positionId")
                    .delete("$.positionName")
                    .put("$", "position", JsonPath.parse("{}").json())
                    .put("$.position", "id", position.getId())
                    .set("$.staffStatus", FIRED.name());
        }

        @Test
        @DisplayName("Throws exception on invalid update data")
        void canValidate() throws Exception {
            //setup invalid staff update json
            staffJson
                    .delete("$.positionId")
                    .delete("$.positionName");

            var staffId = staffJson.read("$.id");

            //Fails on invalid data
            mockMvc
                    .perform(put(PUT_STAFF, staffId)
                            .content(staffJson.jsonString())
                            .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isBadRequest())
                    .andExpect(result -> assertThat(result.getResolvedException())
                            .isInstanceOf(MethodArgumentNotValidException.class));
        }

        @Test
        @DisplayName("Throws exception when trying to update not existing staff")
        void canNotUpdateNotExistingEntity() throws Exception {
            //Change staff to fired status
            mapStaffJsonToFired();

            var staffId = (int) staffJson.read("$.id");

            //Purposefully updating wrong id
            mockMvc
                    .perform(put(PUT_STAFF, staffId + 1)
                            .content(staffJson.jsonString())
                            .contentType(APPLICATION_JSON_VALUE))
                    .andExpect(status().isBadRequest())
                    .andExpect(result -> assertThat(result.getResolvedException())
                            .isInstanceOf(EntityDoesNotExistException.class));
        }
    }

    @Nested
    @DisplayName("Testing DELETE " + STAFF_API)
    @WithMockUser(roles = "ADMIN")
    class DeletingStaffTests {

        @Test
        @DisplayName("Can delete staff")
        void canDeleteExistingStaff(@Value("classpath:json/staff-create.json") Resource staff) throws Exception {
            var staffId = JsonPath
                    .parse(createStaff(getJson(staff, position.getId())))
                    .<Integer>read("$.id");

            //Ensure that staff exist
            var staffJson = mockMvc
                    .perform(get(GET_STAFF, staffId))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(staffId)))
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            mockMvc
                    .perform(delete(DELETE_STAFF, staffId))
                    .andExpect(status().isOk())
                    .andExpect(testKafkaMessage(Action.DELETE, staffJson));

            //Staff should be deleted now
            mockMvc
                    .perform(get(GET_STAFF, staffId))
                    .andExpect(status().isNotFound());
        }

        @Test
        @DisplayName("Throws exception when trying to delete not existing staff")
        void canNotDeleteNotExistingStaff() throws Exception {
            mockMvc
                    .perform(get(GET_STAFF, 1))
                    .andExpect(status().isNotFound());

            mockMvc
                    .perform(delete(DELETE_STAFF, 1))
                    .andExpect(status().isBadRequest())
                    .andExpect(result -> assertThat(result.getResolvedException())
                            .hasCauseInstanceOf(EmptyResultDataAccessException.class));
        }
    }

    @Nested
    @DisplayName("Testing GET page from " + STAFF_API)
    @WithMockUser(roles = "ADMIN")
    class PageStaffTests {

        @Test
        @DisplayName("Paging works")
        void canGetAllStaff(@Value("classpath:json/staff-create.json") Resource staff) throws Exception {
            var staffJson = getJson(staff, position.getId());
            var idList = IntStream
                    .range(0, 10)
                    .map(i -> {
                        try {
                            return JsonPath.parse(createStaff(staffJson)).<Integer>read("$.id");
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .boxed()
                    .sorted()
                    .collect(toUnmodifiableList());

            //Default ordering by id and paging are working
            mockMvc
                    .perform(get(GET_STAFF_PAGE, 0, 5))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.totalPages", is(2)))
                    .andExpect(jsonPath("$.totalElements", is(10)))
                    .andExpect(jsonPath("$.number", is(0)))
                    .andExpect(jsonPath("$.size", is(5)))
                    .andExpect(jsonPath("$.content[*].id", contains(idList.subList(0, 5).toArray())));

            mockMvc
                    .perform(get(GET_STAFF_PAGE, 1, 5))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.totalPages", is(2)))
                    .andExpect(jsonPath("$.totalElements", is(10)))
                    .andExpect(jsonPath("$.number", is(1)))
                    .andExpect(jsonPath("$.size", is(5)))
                    .andExpect(jsonPath("$.content[*].id", contains(idList.subList(5, 10).toArray())));
        }
    }

}
