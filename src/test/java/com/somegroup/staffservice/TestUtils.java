package com.somegroup.staffservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.somegroup.staffservice.messaging.Action;
import com.somegroup.staffservice.messaging.MessageDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.OffsetSpec;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.core.io.Resource;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.web.servlet.ResultMatcher;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@NoArgsConstructor(access = AccessLevel.NONE)
public class TestUtils {
    public static String getJson(Resource resource) throws IOException {
        return FileUtils.readFileToString(resource.getFile(), Charset.defaultCharset());
    }

    public static String getJson(Resource resource, Object... args) throws IOException {
        return String.format(FileUtils.readFileToString(resource.getFile(), Charset.defaultCharset()), args);
    }


    //TODO Отрефачить
    public static <K, V extends MessageDto<?>> ResultMatcher latestMessageMatcher(Action action,
                                                                                  String responseString,
                                                                                  KafkaConsumer<K, V> kafkaConsumer,
                                                                                  AdminClient adminClient,
                                                                                  TopicPartition topicPartition,
                                                                                  ObjectMapper objectMapper) {
        return result -> {
            try {
                var record = TestUtils.getLatestMessage(kafkaConsumer, adminClient, topicPartition);
                var kafkaJson = objectMapper.writeValueAsString(record.value().getData());
                var id = JsonPath.parse(responseString).read("$.id", String.class);

                JSONAssert.assertEquals(kafkaJson, responseString, true);
                assertThat(record.key()).isEqualTo(id);
                assertThat(record.value().getAction()).isEqualTo(action);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    public static <K, V> ConsumerRecord<K, V> getLatestMessage(KafkaConsumer<K, V> kafkaConsumer,
                                                               AdminClient adminClient,
                                                               TopicPartition topicPartition) {
        try {
            var offset = adminClient
                    .listOffsets(Map.of(topicPartition, OffsetSpec.latest()))
                    .all()
                    .get()
                    .values()
                    .stream()
                    .findAny()
                    .orElseThrow()
                    .offset();

            kafkaConsumer.seek(topicPartition, offset > 0 ? offset - 1 : 0);

            return KafkaTestUtils.getSingleRecord(kafkaConsumer, topicPartition.topic());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
